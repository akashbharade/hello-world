//importing modules 
var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var cars = require('cars');
var path = require('path');

var app = express();

//port no 
const port = 3000;

//testing server 
app.get('/', (req, res) => {
    res.send("Hello World");
})



app.listen(port, ()=>{
    console.log('server started at port :' +port);
});